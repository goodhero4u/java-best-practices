package com.mousa.application.algorithms;

import org.testng.Assert;
import org.testng.annotations.Test;

public class BinarySearchUtilTest {

    /*
    1- Find a value that does exist in the array.
    2- Get the correct result in case a value doesn't exist.
    NOTE: This is assuming that the array will be sorted and contains unique values.
     */

    private static int[] arrayOfValues = {7, 8, 9, 500, -44, 1, 330};

    @Test
    private void testValueThatExists() {
        int value = -44;
        int expectedIndexValue = 0;
        int actualIndexValue = BinarySearchUtil.searchIndexOfValue(arrayOfValues, value);

        Assert.assertEquals(actualIndexValue, expectedIndexValue);
    }

    @Test
    private void testValueNoExist() {
        int value = 999;
        int expectedIndexValue = -1;
        int actualIndexValue = BinarySearchUtil.searchIndexOfValue(arrayOfValues, value);

        Assert.assertEquals(actualIndexValue, expectedIndexValue);
    }

    @Test
    private void testValueThatExistsMidArray() {
        int value = 330;
        int expectedIndexValue = 5;
        int actualIndexValue = BinarySearchUtil.searchIndexOfValue(arrayOfValues, value);

        Assert.assertEquals(actualIndexValue, expectedIndexValue);
    }

    @Test
    private void testValueThatExistsEndOfArray() {
        int value = 500;
        int expectedIndexValue = 6;
        int actualIndexValue = BinarySearchUtil.searchIndexOfValue(arrayOfValues, value);

        Assert.assertEquals(actualIndexValue, expectedIndexValue);
    }
}