package com.mousa.application.principles.composition;

public class Penguin implements Bird, NonFlyingAnimal {

    @Override
    public void eat() {
        System.out.println("Penguin eats tiny fish.");
    }

    @Override
    public void move() {
        System.out.println("Penguin walk on their feet");
    }
}
