package com.mousa.application.principles.composition;

public interface Bird {
    void eat();
}
