package com.mousa.application.principles.composition;

public interface NonFlyingAnimal {
    void move();
}
