package com.mousa.application.principles.composition;

public class Parrot implements Bird,FlyingAnimal {

    @Override
    public void eat() {
        System.out.println("The parrot eats food or worms.");
    }

    @Override
    public void fly() {
        System.out.println("The parrot flies.");
    }
}
