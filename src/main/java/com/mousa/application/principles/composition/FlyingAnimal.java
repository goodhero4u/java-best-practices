package com.mousa.application.principles.composition;

public interface FlyingAnimal {
    void fly();
}
