package com.mousa.application.principles.composition;

public class Zoo {

    public static void main(String[] args) {

        Bird parrot = new Parrot();
        parrot.eat();
        ((FlyingAnimal) parrot).fly();

        Bird penguin = new Penguin();
        penguin.eat();
        ((NonFlyingAnimal) penguin).move();

    }
}
