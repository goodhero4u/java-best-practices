package com.mousa.application.principles.dry;

public class JetsFactory extends PlanesFactory {

    @Override
    public String stageThree() {
        return "Assembling rocket engine";
    }

    @Override
    public void deliverToCompany() {
        System.out.println(stageOne());
        System.out.println(stageTwo());
        System.out.println(stageThree());
        System.out.println("Delivers to space agency.");
    }
}
