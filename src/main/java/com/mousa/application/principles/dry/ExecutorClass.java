package com.mousa.application.principles.dry;

public class ExecutorClass {

    public static void main(String[] args){

        PlanesFactory jetPlane = new JetsFactory();
        jetPlane.deliverToCompany();
    }
}
