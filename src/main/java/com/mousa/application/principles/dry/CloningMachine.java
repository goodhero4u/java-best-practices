package com.mousa.application.principles.dry;

public class CloningMachine {

    private final static int BONUS = 15;

    public int calculateBonus(int x, int y) {
        if (x == 1) {
            x++;
        }

        if (y == 5) {
            y++;

        }

        return x + y + BONUS; //15 is the bonus

    }

}
