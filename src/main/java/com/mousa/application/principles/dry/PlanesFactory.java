package com.mousa.application.principles.dry;

public abstract class PlanesFactory {

    String stageOne() {
        return "Assemble wheels";
    }

    String stageTwo() {
        return "Assemble body";
    }

    String stageThree() {
        return "Assemble engines";
    }

    public abstract void deliverToCompany();

}
