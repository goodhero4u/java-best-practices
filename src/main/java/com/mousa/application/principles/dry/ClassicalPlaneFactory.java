package com.mousa.application.principles.dry;

public class ClassicalPlaneFactory extends PlanesFactory {

    @Override
    public void deliverToCompany() {
        System.out.println(stageOne());
        System.out.println(stageTwo());
        System.out.println(stageThree());
        System.out.println("Delivered to company Boeing");

    }
}
