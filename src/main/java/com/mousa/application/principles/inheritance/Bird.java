package com.mousa.application.principles.inheritance;

public class Bird {

    public void eat() {
        System.out.println("The bird eats food.");
    }

    public void fly() {
        System.out.println("The bird flies.");
    }
}
