package com.mousa.application.principles.inheritance;

public class Zoo {

    public static void main(String[] args) {

        Bird parrot = new Parrot("Jeff");

        parrot.fly();
        parrot.eat();

        Bird penguin = new Penguin("Tom");
        penguin.fly();
        penguin.eat();
    }
}
