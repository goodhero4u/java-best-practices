package com.mousa.application.principles.inheritance;

public class Parrot extends Bird {

    String name;

    public Parrot(String name) {
        this.name = name;
    }

    @Override
    public void eat() {
        System.out.println(name+" is a parrot that eats food.");
    }

    @Override
    public void fly() {
        System.out.println(name+ " is a parrot that flies with wings.");
    }
}
