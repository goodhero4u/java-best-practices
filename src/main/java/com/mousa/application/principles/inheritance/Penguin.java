package com.mousa.application.principles.inheritance;

public class Penguin extends Bird {

    String name;

    public Penguin(String name) {
        this.name = name;
    }

    @Override
    public void eat() {
        System.out.println(name + " is a Penguin eats tiny fish.");
    }

    @Override
    public void fly() {
        System.out.println(name + " Penguin has wings but doesn't fly.");
    }
}
