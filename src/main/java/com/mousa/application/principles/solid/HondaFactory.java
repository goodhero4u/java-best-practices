package com.mousa.application.principles.solid;

public class HondaFactory implements CarFactory {

    @Override
    public void collectParts() {

    }

    @Override
    public void assembleParts() {

    }

    @Override
    public void launchRobots() {

    }
}
