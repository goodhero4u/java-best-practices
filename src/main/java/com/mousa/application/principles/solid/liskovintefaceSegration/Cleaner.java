package com.mousa.application.principles.solid.liskovintefaceSegration;

public interface Cleaner {

    void cleanTheFloor();
}
