package com.mousa.application.principles.solid;

public class SchoolManagement {

    //Single Responsibility Principle - True

    public void hireTeachers() {
        System.out.println("School hires teachers.");

    }

    public void procuresStock() {
        System.out.println("School purchases resources for cafeteria.");
    }

}

class Teacher {

    //Single Responsibility Principle - false

    public void teachStudents(){
        System.out.println("The teacher teaches students.");
    }

    public void markExams(){
        System.out.println("Teacher marks student's papers/exams.");
    }

    public void teacherCleansTheClass(){ //Example of violating Single Responsibility Principle.
        System.out.println("The teacher picked up the mob and mobbed the class floor.");
    }

}



