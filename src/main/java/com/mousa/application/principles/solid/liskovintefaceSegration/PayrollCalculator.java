package com.mousa.application.principles.solid.liskovintefaceSegration;

public interface PayrollCalculator {

    double calculatePayroll();
}
