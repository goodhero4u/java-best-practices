package com.mousa.application.principles.solid.liskovintefaceSegration;

public class CalculatorXYZSupplier implements PayrollCalculator {

    @Override
    public double calculatePayroll() {
        System.out.println("XYZ requires power button to be pushed first.");
        return 0;
    }
}
