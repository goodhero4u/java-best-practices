package com.mousa.application.principles.solid.liskovintefaceSegration;

public interface RestaurantStaff {

    void payrollAdminTask();
    void helpingColleagues();
}
