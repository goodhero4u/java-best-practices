package com.mousa.application.principles.solid.liskovintefaceSegration;

public class RestaurantAdmin implements RestaurantStaff {

    @Override
    public void payrollAdminTask() {
        System.out.println("Admin calculates payroll");

        PayrollCalculator payrollCalculator = new CalculatorXYZSupplier();

        System.out.println("Total payroll = " + payrollCalculator.calculatePayroll());

    }

    @Override
    public void helpingColleagues() {

    }
}
