package com.mousa.application.principles.solid;

public interface CarFactory {

    void collectParts();
    void assembleParts();
    void launchRobots();
}
