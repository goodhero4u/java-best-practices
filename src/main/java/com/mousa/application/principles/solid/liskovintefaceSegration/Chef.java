package com.mousa.application.principles.solid.liskovintefaceSegration;

public class Chef implements RestaurantStaff , Cleaner{

    @Override
    public void payrollAdminTask() {

    }

    @Override
    public void helpingColleagues() {

    }

    @Override
    public void cleanTheFloor() {

    }
}
