package com.mousa.application.principles.solid.liskovintefaceSegration;

public class CalculatorABCSupplier implements PayrollCalculator{

    @Override
    public double calculatePayroll() {
        System.out.println("Requires to run once a day.");
        return 0;
    }
}
