package com.mousa.application.generalRecommendations;

import java.util.Optional;

public class OptionalNullHandler {

    public static void main(String[] args) {

        String defaultStudentName = "Sam";

        Optional<String> optionalStudentName = Optional.ofNullable(getApiCall(100));

        System.out.println(optionalStudentName.isPresent());

        System.out.println(optionalStudentName.orElse(defaultStudentName));

        optionalStudentName.orElseThrow(IllegalArgumentException::new);
    }

    private static String getApiCall(int studentId) {
        String studentName = null;

        if (studentId == 1123) {
            return "John";
        }

        if (studentId == 7877) {
            return "Sara";
        }

        return studentName;
    }

}
