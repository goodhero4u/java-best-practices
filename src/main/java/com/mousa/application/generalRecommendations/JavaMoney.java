package com.mousa.application.generalRecommendations;

import org.javamoney.moneta.Money;

import javax.money.MonetaryAmount;
import java.math.BigDecimal;

public class JavaMoney {

    public static void main(String[] args) {

        MonetaryAmount sumOfDollars = Money.of(new BigDecimal("0.9"), "USD");

        for (int i = 0; i < 10; i++) {

            sumOfDollars = sumOfDollars.add(Money.of(new BigDecimal("0.3"), "USD"));

        }

        System.out.println(sumOfDollars);

    }
}
