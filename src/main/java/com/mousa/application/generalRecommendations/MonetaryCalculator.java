package com.mousa.application.generalRecommendations;

import java.math.BigDecimal;

public class MonetaryCalculator {

    public static void main(String[] args) {

        double incorrectMonetaryCalculatedValue = 0.90;

        //Use this if you must use a double
        double doubleValue = 0.90;
        BigDecimal bigDecimalForDoubleValue = BigDecimal.valueOf(doubleValue);

        //In case you are able to use a string
        BigDecimal bigDecimalForString = new BigDecimal("0.90");

        BigDecimal bigDecimalValue = new BigDecimal("0.3");

        for (int i = 0; i < 10; i++) {

            //The correct way to handle precise calculations for big numbers with big decimals
            bigDecimalForString = bigDecimalForString.add(bigDecimalValue);

            //The below is the incorrect way when precision matters and numbers are big
            incorrectMonetaryCalculatedValue += 0.3;
        }

        System.out.println("BigDecimal results = " + bigDecimalForString);

        System.out.println("double's value result = " + incorrectMonetaryCalculatedValue);

    }
}
