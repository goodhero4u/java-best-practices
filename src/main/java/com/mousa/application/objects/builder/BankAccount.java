package com.mousa.application.objects.builder;

import java.time.LocalDateTime;

public class BankAccount {

    private String firstName;
    private String lastName;
    private boolean withInsurance;
    private boolean isOverDraft;
    private LocalDateTime dateOfBirth;
    private boolean isMarried;
    private boolean withInterest;

    private BankAccount(BankAccountBuilder bankAccountBuilder) {
        this.firstName = bankAccountBuilder.firstName;
        this.lastName = bankAccountBuilder.lastName;
        this.withInsurance = bankAccountBuilder.withInsurance;
        this.isOverDraft = bankAccountBuilder.isOverDraft;
        this.dateOfBirth = bankAccountBuilder.dateOfBirth;
        this.isMarried = bankAccountBuilder.isMarried;
        this.withInterest = bankAccountBuilder.withInterest;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public boolean isWithInsurance() {
        return withInsurance;
    }

    public boolean isOverDraft() {
        return isOverDraft;
    }

    public LocalDateTime getDateOfBirth() {
        return dateOfBirth;
    }

    public boolean isMarried() {
        return isMarried;
    }

    public boolean isWithInterest() {
        return withInterest;
    }

    public static class BankAccountBuilder {

        private String firstName;//Required
        private String lastName;//Required
        private boolean withInsurance = true;
        private boolean isOverDraft;
        private LocalDateTime dateOfBirth;
        private boolean isMarried;
        private boolean withInterest;

        public BankAccountBuilder(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public BankAccountBuilder withInsurance(boolean withInsurance) {
            this.withInsurance = withInsurance;
            return this;
        }

        public BankAccountBuilder isOverDraft(boolean isOverDraft) {
            this.isOverDraft = isOverDraft;
            return this;
        }

        public BankAccountBuilder dateOfBirth(LocalDateTime dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
            return this;
        }

        public BankAccountBuilder isMarried(boolean isMarried) {
            this.isMarried = isMarried;
            return this;
        }

        public BankAccountBuilder withInterest(boolean withInterest) {
            this.withInterest = withInterest;
            return this;
        }

        public BankAccount build() {
            BankAccount newBankAccount = new BankAccount(this);
            return newBankAccount;
        }

    }

}
