package com.mousa.application.objects.builder;

public class Bank {

    public static void main(String[] args) {

        BankAccount bankAccount = new BankAccount.BankAccountBuilder("Tom", "Jean").withInsurance(false).build();

        System.out.println(bankAccount.isWithInsurance());

    }
}
