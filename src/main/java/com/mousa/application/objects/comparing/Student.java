package com.mousa.application.objects.comparing;

import java.util.Objects;

public class Student implements Comparable {

    private String firstName;
    private String id;
    private int studentScore;

    public Student(String firstName, String id, int studentScore) {
        this.firstName = firstName;
        this.id = id;
        this.studentScore = studentScore;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getId() {
        return id;
    }

    public int getStudentScore() {
        return studentScore;
    }

    @Override
    public boolean equals(Object theOtherObject){
        if(theOtherObject == null){
            return false;
        }

        if(theOtherObject instanceof Student otherStudent){
            return this.firstName.equalsIgnoreCase(otherStudent.getFirstName())
                    && this.id.equals(otherStudent.getId());
        }

        return false;
    }

    @Override
    public int hashCode(){
        return Objects.hash(firstName,id);
    }

    @Override
    public String toString() {
        return "Student{" +
                "firstName='" + firstName + '\'' +
                ", id='" + id + '\'' +
                ", studentScore=" + studentScore +
                '}';
    }

    @Override
    public int compareTo(Object otherStudentObject) {
        return Integer.compare(studentScore,((Student)otherStudentObject).getStudentScore());
    }
}
