package com.mousa.application.objects.comparing;

import java.util.*;

public class School {

    public static void main(String[] args) {
        Student tom = new Student("Tom", "1123", 20);

        Student differentTom = new Student("Tom", "11243", 20);

        System.out.println(tom.equals(differentTom));

        List<Student> records = new ArrayList<>();

        records.add(new Student("John", "223", 20));
        records.add(new Student("John", "223", 25));
        records.add(new Student("Sarah", "0013", 40));
        records.add(new Student("Sarah", "0013", 50));

        Set<Student> studentSet = new HashSet<>();

        for (Student s : records) {
            studentSet.add(s);
        }

        System.out.println(studentSet.size());

        Student[] studentsRecordsToSort = {
                new Student("Tom","334",405),
                new Student("Sarah","23444",60),
                new Student("John","2344",9900),
                new Student("David","0933",195)};

        Arrays.sort(studentsRecordsToSort);

        int lastIndex = studentsRecordsToSort.length-1;

        System.out.println("The student with the highest score is \n"+studentsRecordsToSort[lastIndex].toString());

    }

}
