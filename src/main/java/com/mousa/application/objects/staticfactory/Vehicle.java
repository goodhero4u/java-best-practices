package com.mousa.application.objects.staticfactory;

public interface Vehicle {

    void printCylinders();
    void printCarType();
    void printCarName();

}
