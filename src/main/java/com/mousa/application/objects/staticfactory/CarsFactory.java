package com.mousa.application.objects.staticfactory;

public class CarsFactory {

    public static void main(String[] args) {

        Vehicle honda = getVehicle(4, "Honda", VehicleType.CAR);

        honda.printCarType();

    }

    private static Vehicle getVehicle(int cylinders, String name, VehicleType type) {

        if (type.equals(VehicleType.CAR)) {
            return new Car(cylinders, name);
        }

        if (type.equals(VehicleType.TRUCK)) {
            return new Truck(cylinders, name);
        }

        throw new IllegalStateException("The vehicle type passed doesn't exist");

    }

}
