package com.mousa.application.objects.staticfactory;

public class Truck implements Vehicle {

    private int cylinders;
    private String name;

    public Truck(int cylinders, String name) {
        this.cylinders = cylinders;
        this.name = name;
    }

    @Override
    public void printCylinders() {
        System.out.println(cylinders);
    }

    @Override
    public void printCarType() {
        System.out.println(this.getClass().getName());
    }

    @Override
    public void printCarName() {
        System.out.println(name);
    }
}
