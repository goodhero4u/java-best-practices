package com.mousa.application.java16;

import java.util.Objects;

public class RecordsLecture {

    public static void main(String[] args) {

        ModernCarDTO car1 = new ModernCarDTO(2022, 1234);

        ModernCarDTO car2 = new ModernCarDTO(2024, 1234);

        System.out.println(car1.equals(car2));

    }

    record ModernCarDTO(int model, int plateNumber) {
    }

}

class TraditionalCarDTO {

    private final int model;

    private final int plateNumber;

    public TraditionalCarDTO(int model, int plateNumber) {
        this.model = model;
        this.plateNumber = plateNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TraditionalCarDTO that = (TraditionalCarDTO) o;
        return model == that.model && plateNumber == that.plateNumber;
    }

    @Override
    public int hashCode() {
        return Objects.hash(model, plateNumber);
    }

    @Override
    public String toString() {
        return "TraditionalCarDTO{" +
                ", plateNumber=" + plateNumber +
                '}';
    }
}