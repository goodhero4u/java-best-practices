package com.mousa.application.algorithms;

import java.util.Arrays;

public class BinarySearchUtil {

    /*
    1- The values need to be unique in the array.
    2- The values need to be sorted in an ascending order.
     */

    public static int searchIndexOfValue(int[] array, int value) {
        Arrays.sort(array);
        int start = 0;
        int end = array.length;

        while (start < end) {
            int midPoint = (start + end) / 2;

            if (array[midPoint] == value) {
                return midPoint;
            }

            if (array[midPoint] < value) {
                start = midPoint + 1;
                continue;
            }

            end = midPoint;
        }

        return -1;
    }
}
