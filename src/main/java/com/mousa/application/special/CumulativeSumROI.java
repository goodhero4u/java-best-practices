package com.mousa.application.special;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Random;

public class CumulativeSumROI {

    private static final BigDecimal ONE_ADJUSTED = BigDecimal.ONE.setScale(10, RoundingMode.HALF_EVEN);

    public static void main(String[] args) {

        BigDecimal[] returnOnInvestments = new BigDecimal[100]; //ROI for each month independent of others

        populateArray(returnOnInvestments);

        //// USING PARALLEL PREFIX FROM ARRAYS ///

        long start = System.currentTimeMillis();

        setCumulativeGrowthUsingArrays(returnOnInvestments);

        long time = System.currentTimeMillis() - start;

        System.out.println("using parallelPrefix took = " + time);


        /// USING LINEAR OPERATION ///

        populateArray(returnOnInvestments); //Resetting ROI

        start = System.currentTimeMillis(); // Resetting timer

        setCumulativeGrowthLinearLoop(returnOnInvestments);

        time = System.currentTimeMillis() - start;
        System.out.println("using linear operation took = " + time);

    }

    private static void populateArray(BigDecimal[] arr) {
        Random random = new Random();
        for (int i = 0; i < arr.length; i++) {
            double randomValue = random.nextDouble() * (0.15 - (-0.05)) + (-0.05);
            if (arr[i] == null) {
                arr[i] = BigDecimal.valueOf(randomValue);
                continue;
            }

            double diff = arr[i].doubleValue() - 0;

            if (diff > 0) {
                arr[i] = arr[i].min(BigDecimal.valueOf(diff));
                continue;
            }

            arr[i] = arr[i].add(BigDecimal.valueOf(diff));
        }
    }

    private static void setCumulativeGrowthUsingArrays(BigDecimal[] returnOnInvestments) {
        Arrays.parallelPrefix(returnOnInvestments, CumulativeSumROI::getCumulativeRowForPeriod);
    }

    private static void setCumulativeGrowthLinearLoop(BigDecimal[] returnOnInvestments) {
        for (int i = 1; i < returnOnInvestments.length; i++) {
            BigDecimal previousROI = returnOnInvestments[i - 1];
            BigDecimal currentROI = returnOnInvestments[i];
            returnOnInvestments[i - 1] = getCumulativeRowForPeriod(previousROI, currentROI);
        }
    }

    private static BigDecimal getCumulativeRowForPeriod(BigDecimal previousROI, BigDecimal currentROI) {
        BigDecimal adjustedPreviousROI = previousROI.add(ONE_ADJUSTED);
        BigDecimal adjustedCurrentROI = currentROI.add(ONE_ADJUSTED);
        return adjustedPreviousROI.multiply(adjustedCurrentROI).subtract(ONE_ADJUSTED);
    }
}
