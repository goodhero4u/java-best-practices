package com.mousa.application.special;

import java.util.*;

public class SetsDisjoint {

    public static void main(String[] args) {

        // Checking disjoint for built-in Java objects

        Set<Integer> items1 = new HashSet<>(Arrays.asList(1, 2, 3, 4));

        Set<Integer> items2 = new HashSet<>(Arrays.asList(9, 7));

        Set<Integer> items3 = new HashSet<>(Arrays.asList(10, 20, 44));

        // Comparing sets

        boolean isTwoSetsDisjoint = Collections.disjoint(items1, items2);

        System.out.println("Are first two items' sets disjoint? " + isTwoSetsDisjoint);

        boolean isThreeSetsDisjoint = Collections.disjoint(items1, items2)
                && Collections.disjoint(items2, items3)
                && Collections.disjoint(items1, items3);

        System.out.println("Are all three items' sets disjoint? " + isThreeSetsDisjoint);

        // Comparing lists

        List<Integer> itemsList1 = new ArrayList<>(Arrays.asList(77, 99, 101));

        List<Integer> itemsList2 = new ArrayList<>(Arrays.asList(20, 30, 40, 90));

        boolean isTwoItemListDisjoint = Collections.disjoint(itemsList1, itemsList2);

        System.out.println("Are the two lists disjoint? " + isTwoItemListDisjoint);

        // Comparing lists and sets

        boolean isCollectionAndListDisjoint = Collections.disjoint(items1, itemsList1);

        System.out.println("We can check cross collections such as list and set\nAre the list and set disjoint? " + isCollectionAndListDisjoint);

        // Checking disjoint for custom objects

        Set<Car> carsSet1 = new HashSet<>(Arrays.asList(new Car(124), new Car(445)));

        Set<Car> carsSet2 = new HashSet<>(Arrays.asList(new Car(7444), new Car(987), new Car(9999)));

        boolean isCarSetsDisjoint = Collections.disjoint(carsSet1, carsSet2);

        System.out.println("Are cars sets disjoint? " + isCarSetsDisjoint);

    }

}

class Car {

    private final int plateNumber;

    public Car(int plateNumber) {
        this.plateNumber = plateNumber;
    }

    @Override
    public boolean equals(Object o) { //It is extremely important to override equals and hashCode for custom objects
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return plateNumber == car.plateNumber;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(plateNumber);
    }
}
