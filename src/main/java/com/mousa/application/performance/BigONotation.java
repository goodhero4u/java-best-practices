package com.mousa.application.performance;

import java.util.Arrays;

public class BigONotation {

    public static void main(String[] args) {

        // O(1)
        int[] numsArray = {1, 2, 34, 7, 100};
        int expectedNumber = numsArray[2];

        //O(logn)
        boolean valueExists = Arrays.binarySearch(numsArray, 2) > -1;

        //O(n)
        for (int i = 0; i < numsArray.length; i++) {
            System.out.println(numsArray[i]);
        }

        //O(nlogn)
        for (int u : numsArray) {

            int findValue = Arrays.binarySearch(numsArray, 100);

        }


        // O(n^2)
        for (int num : numsArray) {

            for (int anotherNum : numsArray) {

            }
        }


    }
}
