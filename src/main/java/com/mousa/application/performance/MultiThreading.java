package com.mousa.application.performance;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;

public class MultiThreading {

    private final static java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(MultiThreading.class.getName());

    private final static String PIZZA_READY = "Order completed - We finished preparing all pizzas! ";

    private final static String TIME_TAKEN = "Order time (in milliseconds) = ";

    ///////////////////////// BELOW IS MULTI THREADED IMPLEMENTATION /////////////////////////////

    public static void main(String[] args) {

        long timeOrderMade = System.currentTimeMillis();

        ExecutorService executor = Executors.newFixedThreadPool(10); //We allocated 10 threads in total to the pool

        executor.submit(() -> {
            // You can put here the task that you wish to run
            preparePizzas(50, timeOrderMade);
        });

        executor.submit(() -> {
            // You can put here the task that you wish to run
            preparePizzas(100000000, timeOrderMade);
        });

        executor.submit(() -> {
            // You can put here the task that you wish to run
            preparePizzas(200000000, timeOrderMade);
        });

        executor.submit(() -> {
            // You can put here the task that you wish to run
            preparePizzas(300000000, timeOrderMade);
        });

        executor.shutdown(); // You must add this line. Otherwise, the JVM will keep running
    }

    ///////////////////////// BELOW IS SINGLE THREADED IMPLEMENTATION /////////////////////////////

//    public static void main(String[] args) {
//
//        long timeOrderMade = System.currentTimeMillis();
//
//        preparePizzas(50, timeOrderMade);
//
//        preparePizzas(100000000, timeOrderMade);
//
//        preparePizzas(200000000, timeOrderMade);
//
//        preparePizzas(300000000, timeOrderMade);
//
//    }

    private static void preparePizzas(int number, long timeOrderPlacesInMillis) {

        for (int i = 0; i <= number; i++) ;

        StringBuilder stringBuilder = new StringBuilder(PIZZA_READY);
        stringBuilder.append(number);

        LOGGER.log(Level.INFO, stringBuilder.toString());

        stringBuilder = new StringBuilder(TIME_TAKEN);

        long totalDuration = System.currentTimeMillis() - timeOrderPlacesInMillis;

        stringBuilder.append(totalDuration);
        LOGGER.log(Level.INFO, stringBuilder.toString());
    }
}