package com.mousa.application.performance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StringConcatenator {

    public static void main(String[] args) {

        List<String> wordsList = new ArrayList<>(Arrays.asList("My", "name", "is", "Dev."));


        long startTimeString = System.currentTimeMillis();

        String fullSentence = "";

        for (String word : wordsList) {
            fullSentence += word;
            fullSentence += " ";
        }

        long endTimeString = System.currentTimeMillis();

        System.out.println("Execution for String in milliseconds: " + (endTimeString - startTimeString));


        long startTimeStringBuilder = System.currentTimeMillis();

        StringBuilder fullSentenceUsingStringBuilder = new StringBuilder();

        for (String word : wordsList) {
            fullSentenceUsingStringBuilder.append(word);
        }

        long endTimeStringBuilder = System.currentTimeMillis();

        System.out.println("Execution for StringBuilder in milliseconds: " + (endTimeStringBuilder - startTimeStringBuilder));

    }
}
