package com.mousa.application.performance;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static com.mousa.application.performance.CountDownLatchLecture.sleepRandomTime;

public class CountDownLatchLecture {

    private final static int COUNT = 3;

    public static void main(String[] args) throws InterruptedException {

        CountDownLatch countDownLatch = new CountDownLatch(COUNT);

        ExecutorService executor = Executors.newFixedThreadPool(3);

        executor.submit(new APIInitializer(countDownLatch));
        executor.submit(new DatabaseInitializer(countDownLatch));
        executor.submit(new CacheManager(countDownLatch));

        boolean isComplete = countDownLatch.await(60, TimeUnit.SECONDS);

        if (isComplete) {
            System.out.println("Web application is ready to serve requests!\nAll initialization is complete!");
        }

        executor.shutdown();

    }

    public static void sleepRandomTime(String finishMessage) {

        Random random = new Random();
        int maxSleepMillis = 6000;
        int randomSleepMillis = random.nextInt(maxSleepMillis);

        try {
            Thread.sleep(randomSleepMillis);
            System.out.println(finishMessage);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }
}

class DatabaseInitializer implements Runnable {

    private final CountDownLatch countDownLatch;

    public DatabaseInitializer(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void run() {
        try {
            sleepRandomTime("Initializing database connection complete.");
        } finally {
            countDownLatch.countDown();
        }

    }
}

class APIInitializer implements Runnable {

    private final CountDownLatch countDownLatch;

    public APIInitializer(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void run() {
        try {
            sleepRandomTime("Pinging API to verify status.");
        } finally {
            countDownLatch.countDown();
        }
    }
}

class CacheManager implements Runnable {

    private final CountDownLatch countDownLatch;

    public CacheManager(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void run() {
        try {
            sleepRandomTime("Caching configurations.");
        } finally {
            countDownLatch.countDown();
        }
    }
}
