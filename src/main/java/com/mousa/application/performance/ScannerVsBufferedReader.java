package com.mousa.application.performance;

import java.io.*;
import java.util.Scanner;

public class ScannerVsBufferedReader {

    public static void main(String[] args) throws IOException {

        String largeFileNamePath = "tooManyStrings.txt";

        //Scanner Test
        long startScannerInMillis = System.currentTimeMillis();

        File file = new File(largeFileNamePath);

        Scanner scanner = new Scanner(file);

        while (scanner.hasNext()) {
            System.out.println(scanner.nextLine());
        }

        long endScannerTimeInMillis = System.currentTimeMillis();

        long scannerDurationInMillis = endScannerTimeInMillis - startScannerInMillis;


        //BufferedReader Test
        long startBufferedReaderInMillis = System.currentTimeMillis();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(largeFileNamePath))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }

        }

        long endBufferedReaderInMillis = System.currentTimeMillis();

        long durationBufferedReaderInMillis = endBufferedReaderInMillis - startBufferedReaderInMillis;

        System.out.println("//////////////////////////////////////////////////////////////////////////");

        System.out.println("Results for Scanner = " + scannerDurationInMillis);
        System.out.println("Results for BufferedReader = " + durationBufferedReaderInMillis);

    }
}
