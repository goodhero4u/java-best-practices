package com.mousa.application.performance;

import java.util.Arrays;

public class SearchProblem {

    public static void main(String[] args) {

        int[] array = {1, 9, -10, 99, 239};//Values in this case are unique.

        // 1- Find a negative value , then find value = 239 and then find value = 1000

        //Slow solution - Performance = O(n)
        for (int i = 0; i < array.length; i++) {

            if (array[i] < 0) {
                System.out.println("We found a negative value.");
            }
        }

        //Better solution - Below solution has a time complexity of O(Logn)
        Arrays.sort(array); //Setup for solving problems.

        System.out.println("Sorted array below");

        System.out.println(Arrays.toString(array));

        System.out.println("-------------------------------");

        //Requirements of binary search
        //1- The array has to be sorted.
        //2- The values are unique.

        boolean negativeValue = array[0] < 0 ? true : false;
        int indexOfValue = Arrays.binarySearch(array, 239);
        int indexOfValueTwo = Arrays.binarySearch(array, 1000);

        System.out.println(negativeValue);
        System.out.println(indexOfValue);
        System.out.println(indexOfValueTwo);

    }
}
