package com.mousa.application.performance;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FileWriterVsBufferedWriter {

    public static void main(String[] args) {

        long startTimeFileWriter = System.currentTimeMillis();

        //1- Test with simple String
        String simpleTestString = "Hello world";

        ///////////////////////////////// FileWriter test below ////////////////////////////////////
        try (FileWriter fileWriter = new FileWriter("FileWriterResult.txt")) {

            fileWriter.write(simpleTestString);

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }

        long endTimeFileWriter = System.currentTimeMillis();

        long totalMilliSecondsFileWriter = endTimeFileWriter - startTimeFileWriter;

        /////////////////////////////////////// BufferedWriter test below ////////////////////////////////////////

        long startTimeBufferedWriter = System.currentTimeMillis();

        try (FileWriter fileWriterForBufferedWriter = new FileWriter("BufferedWriterResult.txt")) {

            try (BufferedWriter bufferedWriter = new BufferedWriter(fileWriterForBufferedWriter)) {
                bufferedWriter.write(simpleTestString);
            }

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }

        long endTimeBufferedWriter = System.currentTimeMillis();

        long totalMilliSecondsBufferedWriter = endTimeBufferedWriter - startTimeBufferedWriter;

        System.out.println("Total time in millis for FileWriter = " + totalMilliSecondsFileWriter);
        System.out.println("Total time in millis for BufferedWriter = " + totalMilliSecondsBufferedWriter);
        ///////////////////////////////////////////// 2nd Test Below ////////////////////////////////////////////

        //2- Testing with larger data
        String[] arr = {"Hello world", "how are you", "good performance", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "how are you", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance", "good performance"};

        //////////////////////////// FileWriter large array test below /////////////////////////////////

        long startTimeFileWriterLargeArray = System.currentTimeMillis();

        try (FileWriter fileWriterLargeArray = new FileWriter("FileWriterResultLargeArray.txt")) {

            for (String string : arr) {
                fileWriterLargeArray.write(string);
            }

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }

        long endTimeFileWriterLargeArray = System.currentTimeMillis();

        long totalTimeFileWriterLargeArray = endTimeFileWriterLargeArray - startTimeFileWriterLargeArray;

        /////////////////////////////////// BufferedWriter large array test below //////////////////////////////////

        long startTimeBufferedWriterLargeArray = System.currentTimeMillis();

        try (FileWriter fileWriterLargeArrayForBufferedWriter = new FileWriter("BufferedWriterResultLargeArray.txt")) {
            try (BufferedWriter bufferedWriterLargeArray = new BufferedWriter(fileWriterLargeArrayForBufferedWriter)) {

                for (String string : arr) {
                    bufferedWriterLargeArray.write(string);
                }

            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }

        long endTimeBufferedWriterLargeArray = System.currentTimeMillis();

        long totalTimeBufferedWriterLargeArray = endTimeBufferedWriterLargeArray - startTimeBufferedWriterLargeArray;

        System.out.println("The total time in millis for FileWriter with large array = "+totalTimeFileWriterLargeArray);
        System.out.println("The total time in millis for BufferedWriter with large array = "+totalTimeBufferedWriterLargeArray);
    }
}
