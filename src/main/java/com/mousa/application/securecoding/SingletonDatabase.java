package com.mousa.application.securecoding;

public class SingletonDatabase {

    private SingletonDatabase(){

    }

    private static volatile transient SingletonDatabase SINGLETON_DATABASE;

    public static SingletonDatabase getSingletonInstance() {

        synchronized (SingletonDatabase.class) {

            if (SINGLETON_DATABASE == null) {
                SINGLETON_DATABASE = new SingletonDatabase();
            }

        }

        return SINGLETON_DATABASE;
    }
}
