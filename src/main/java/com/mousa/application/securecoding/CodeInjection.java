package com.mousa.application.securecoding;

import java.sql.*;
import java.util.Scanner;

public class CodeInjection {

    public static Connection connObj;
    public static String JDBC_URL = "jdbc:sqlserver://localhost\\SQLEXPRESS;Database=master;Trusted_Connection=True;";

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Input userId");

        String userId = scanner.nextLine();

        //executeSQLStatement(userId);

    }

    private static void executeSQLStatement(String userInputId) {

        try {
            //PreparedStatement as the best practice for handling SQL in java.
            //Do not use ordinary statement as discussed in the video lesson.
            PreparedStatement preparedStatement = connObj.prepareStatement("SELECT name, salary FROM testTable where userId = ?");
            preparedStatement.setString(1,userInputId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String name = resultSet.getString("name");
                long salary = resultSet.getLong("salary");

                System.out.println(name);
                System.out.println(salary);
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }

    }


    private static void getDbConnection() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            connObj = DriverManager.getConnection(JDBC_URL);
            if (connObj != null) {
                DatabaseMetaData metaObj = (DatabaseMetaData) connObj.getMetaData();
                System.out.println("Driver Name?= " + metaObj.getDriverName() + ", Driver Version?= " + metaObj.getDriverVersion() + ", Product Name?= " + metaObj.getDatabaseProductName() + ", Product Version?= " + metaObj.getDatabaseProductVersion());
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
        }
    }

}
