package com.mousa.application.securecoding;

import java.time.LocalDateTime;
import java.util.logging.Level;

public class LoggingApplication {

    static java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(LoggingApplication.class.getName());

    public static void main(String[] args) {

        Customer customerLoggingIn = new Customer("Tom", LocalDateTime.now(), "12326343dfd", "1234");

        LOGGER.log(Level.INFO, "A customer has just logged into the application.");
        LOGGER.log(Level.INFO, customerLoggingIn.toString());

    }

}
