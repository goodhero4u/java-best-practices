package com.mousa.application.securecoding;

import org.identityconnectors.common.security.GuardedString;

public class PasswordStore {

    public static void main(String[] args) {

        String username = "John990";

        String password = "abc";

        String creditCardPin = "787";

        String socialSecurityNumber = "123456789";

        String dateOfBirth = "01/01/2000";

        //First approach is to use char[]

        char[] passwordArray = {'a','b','c','t'};

        //Second approach is to use a third party solution

        GuardedString guardedString = new GuardedString(passwordArray);
        //GuardedString is provided by org.identityconnectors library you can find more details about the dependency in the pom.xml file.

        System.out.println(guardedString.toString());

        guardedString.access(new GuardedString.Accessor() {
            @Override
            public void access(char[] chars) {
                System.out.println(chars);
            }
        });

        guardedString.dispose();

        passwordArray = null;

        guardedString.access(new GuardedString.Accessor() {
            @Override
            public void access(char[] chars) {
                System.out.println(chars);
            }
        });

    }
}
