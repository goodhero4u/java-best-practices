package com.mousa.application.securecoding;

import java.time.LocalDateTime;

public class Customer {

    private String name;
    private LocalDateTime dateOfBirth;
    private String socialSecurityNumber;
    private String last4CreditCardDigits;

    public Customer(String name, LocalDateTime dateOfBirth, String socialSecurityNumber, String last4CreditCardDigits) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.socialSecurityNumber = socialSecurityNumber;
        this.last4CreditCardDigits = last4CreditCardDigits;
    }

    //Always make sure to remove sensitive information when overriding the toString()
    @Override
    public String toString() {
        return "Customer{" +
                "name='" + name + '\'' +'}';
    }
}
