package com.mousa.application.securecoding;

import java.util.Scanner;

public class StringSanitization {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input data");

        while(true){
            String firstName = scanner.nextLine();

            String firstNameTrimmed = firstName.trim();

            if(firstNameTrimmed.length() < 100 && firstNameTrimmed.matches("[A-Za-z]+")){
                System.out.println("The input is valid.");
            }else{
                System.out.println("The input is invalid. Please try again.");
            }

        }

    }
}
